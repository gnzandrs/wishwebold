# Introducción #

En este repositorio se encuentran almacenada la versión web backend de la aplicación Wish, con sus respectivas ramas. Cada rama posee un flujo de trabajo diferente a partir de uno base en común. La construcción de diferentes ramas se utiliza para ir desarrollando en paralelo ya sea por mejoras de una parte del sistema, alternativas de construcción que son sometidas a evaluación o corrección de bugs.

### ¿Para que es éste repositorio? ###

* Wiki en donde se debe explicar la explicación técnica de como se encuentra construida la aplicación y estructurada en el proyecto en particular, de modo que cualquier persona que posea conocimientos del lenguaje se pueda poner a desarrollar bajo los estarandares que fueron definidos para la aplicación. 
* Versiones de las aplicaciones con sus respectivos respaldos. 
* Para respaldos mas generales se deben incluir dentro del menú "Descargas" adjuntado en un .rar con el siguiente formato:
WebWishV15Bkp.rar

en donde las siglas corresponderían a lo siguiente:
[Tipo de aplicación web o móvil] + [Nombre proyecto] + [Version] + Bkp

### Instalación ###

Los tutoriales de instalación se encuentran construidos en el wikipedia correspondiente.

### ¿Con quien Hablo? ###

* Cualquier duda favor de escribir a la persona correspondiente:
Versión Web BackEnd/FrontEnd: andresvergara.cl@gmail.com
Version Movil BackEnd/FrontEnd: pablo.rebolledo@gmail.com