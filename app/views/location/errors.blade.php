<div>
@if ($errors->any())
	<div data-alert class="alert-box warning radius">
		<strong>Por favor corrige los siguientes errores:</strong>
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		<a href="#" class="close">&times;</a>
		<input type='button' value='Cerrar' class="small round button" />
	</div>
@endif
</div>