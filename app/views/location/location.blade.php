<?php
        $form_data2 = array('route' => 'location/store', 'method' => 'POST', 'data-abide' => true);      
?>

{{ Form::model($location, $form_data2, array('role' => 'form')) }}

	<div id="contenidoUbicacion">
		<center>   
      		<input type="text"  name="location_name" id="location_name" placeholder="Introduce un nombre" required data-invalid/>
			<small class="error">El nombre es requerido.</small>
			<input type="button" class="tiny round button" value="Guardar" id="btnUbicacion" onclick='javascript:setLocationName(location_name.value);'/>
		<center>
	</div>

{{ Form::close() }}

