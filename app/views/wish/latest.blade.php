<style>
    .ui-menu { width: 150px; }
</style>
<script>
    $(function() {
        $( "#wishMenu" ).menu();
    });
</script>
<div class="row">

    <h3>Añadidos Recientemente</h3>

</div>

<br>

<ul class="small-block-grid-3">

    @foreach ($wishs as $wish)

    <li>

        <div class="small-12 columns wishBar">

            <!-- imagen -->
            <div class="small-12 columns">

                <a href="{{ route('wish/show', $wish->id) }}"><img src="{{ asset($wish->WishImages->first()->thumb_path) }}"></a>

            </div>

            <div class="small-12 columns wishButtons">

                <a data-id="{{ $wish->id }}" class="wishAddButton"><img src="{{ asset('assets/img/buttons/add.png') }}"></a>

                <a data-id="{{ $wish->id }}" class="wishSocialButton"><img src="{{ asset('assets/img/buttons/shuffle.png') }}"></a>

                <a data-id="{{ $wish->id }}" class="wishLoveButton"><img src="{{ asset('assets/img/buttons/heart.png') }}"></a>

                <div id="wish-{{ $wish->id }}" style="position: relative; right: 30px; bottom: 20px;"></div>

            </div>

            <div class="small-12 columns text-left wishDescription">

                {{ $wish->description }}

            </div>

            <hr>

            <!-- panel -->
            <div  class="small-12 columns">

                <div class="small-4 columns wishAvatar text-left">

                    <img src="{{ asset($wish->WishList->User->UserImage->path) }}">

                </div>

                <div class="small-8 columns">

                    <b>{{ $wish->WishList->User->username }}</b>
                    <br/>
                    {{ $wish->WishList->User->name }} {{ $wish->WishList->User->lastname }}

                </div>

            </div>

        </div>

    </li>

    @endforeach

</ul>

<script src="{{ asset('assets/js/modules/wish/latest.js') }}"></script>