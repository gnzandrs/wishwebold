@if (count($wishs) == 0)
<div>

    <h4>No has añadido ningun deseo aún.</h4>

    <img src="{{ asset('assets/img/meditacion.jpg') }}" width="100" height="250">

    <br>

    <medium>Liberate de todo deseo decia buda...</medium>

    <br>

</div>
@else
<table class="table table-striped">

    <tr>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Precio</th>
        <th></th>
    </tr>

    @foreach ($wishs as $wish)
    <tr>
        <td>{{ $wish->description }}</td>
        <td>{{ $wish->reference }}</td>
        <td>{{ $wish->price }}</td>
        <td>
            <a href="{{ route('wish/show', $wish->id) }}" class="button [tiny small large]">Ver</a>
            <a data-id="{{ $wish->id }}" class="button [tiny small large] btnEditar">Editar</a>
            <a data-id="{{ $wish->id }}" class="button [tiny small large] btnBorrar">Borrar</a>
        </td>
    </tr>
    @endforeach

</table>

<br>
<!-- BOTON ACEPTAR -->
<div class="large-3 columns text-center">

   <!-- <a id="btnGuardar" href="{{ URL::previous() }}" class="big round button">Guardar</a> -->

</div>
<!-- FIN BOTON ACEPTAR -->

@endif

<script src="{{ asset('assets/js/modules/wish/wishlist.js') }}"></script>