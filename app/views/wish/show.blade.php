@extends ('layout')

@section ('title') Crear Lista @stop

@section ('content')

<style>
    #map{
        display: block;
        width: 100%;
        height: 350px;
        margin: 4em auto;
    }
</style>

<link rel="stylesheet" href="{{ asset('assets/css/basic.css') }}" />

<div class="row text-center">

    <div class="small-12 large-centered columns text-center">

        <div class="row">

            <!-- foto deseo -->
            <div class="small-4 columns text-left">

                <div class="row">

                    <!-- galeria -->
                    <?php $n = 1; ?>

                    <ul class="clearing-thumbs clearing-feature" data-clearing>

                        @foreach ($wish->WishImages as $wishImage)

                           @if ($n == 1)

                            <li class="clearing-featured-img">

                            <?php $n++; ?>

                           @else

                            <li>

                           @endif

                             <a href="{{ asset($wishImage->path) }}"><img src="{{ asset($wishImage->thumb_path) }}"></a>

                            </li>

                        @endforeach

                    </ul>

                    <!-- fin galeria -->
                </div>

                <div class="row">

                    <div class="small-4 columns">

                        Descripcion:

                    </div>

                    <div class="small-8 columns">

                        {{ $wish->description }}

                    </div>

                </div>

                <div class="row">

                    <div class="small-4 columns">

                        Referencia:

                    </div>

                    <div class="small-8 columns">

                        {{ $wish->reference }}

                    </div>

                </div>

                <div class="row">

                    <div class="small-4 columns">

                        Precio:

                    </div>

                    <div class="small-8 columns">

                        {{ $wish->price }}

                    </div>

                </div>

            </div>
            <!-- fin foto deseo -->

            <!-- descripcion deseo -->
            <div class="small-8 columns text-left">

                <!-- MAPA -->
                <div id="map" style="margin: 0 !important;">
                </div>

            </div>
            <!-- fin descripcion deseo -->

        </div>

        <br>
        <hr>
        <br>

        <div class="row">

            <div class="small-12 columns">

                <strong>Compartir en:</strong>

                <center><img src="{{ asset('assets/img/sociales.png') }}" ></center>
                <br>
                <br>
                Deseo de: <strong>{{ $wish->WishList->User->username }}</strong>
                <br>
                <br>
                <div class="row">

                    <img class="avatar" src="{{ asset($wish->WishList->User->UserImage->path) }}">

                </div>

                <br>
                <br>
                <a href="{{ route('wishlist/edit', $wish->WishList->id) }}"><- Ver Lista</a>
                <br>
                <a href="{{ route('user/show', $wish->WishList->User->id) }}"><- Ver Perfil</a>

            </div>

        </div>

    </div> <!--- end 12 columnas -->
</div> <!-- end row -->
<!-- PARAMETROS -->
<input type="hidden" name="hdLocationId" id="hdLocationId" value="{{ $wish->Location }}">
<br>
<br>
<br>
<br>
<br>
<!-- libreria gmaps js -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ asset('assets/js/gmaps.js') }}"></script>
<script src="{{ asset('assets/js/modules/wish/show.js') }}"></script>
@stop