<nav id="menu" class="top-bar hide-for-small" data-topbar>

    <ul class="title-area">
        <li class="name">
            <h1><a href="{{ route('home') }}">SmartGift</a></h1>
        </li>
    </ul>

    @if (Auth::check())

    <section class="top-bar-section">
        <ul class="right">
            <li class="divider"></li>
            <li class="has-dropdown">
                <a>Perfil</a>
                <ul class="dropdown">
                    <li><a href="{{ route('user/show', Auth::user()->id) }}">Ver</a></li>
                </ul>
            </li>
            <li class="divider"></li>
            <li class="has-dropdown">
                <a>Listas</a>
                <ul class="dropdown">
                    <li><a href="{{ route('wishlist/create') }}">Crear</a></li>
                    <li><a href="{{ route('wishlist/show') }}">Mis Listas</a></li>
                </ul>
            </li>
            <li class="divider"></li>
            <li class="has-dropdown">
                <a>Configuracion</a>
                <ul class="dropdown">
                    <li><a href="{{ route('user/option') }}">Opciones</a></li>
                    <li><a href="{{ route('user/profile') }}">Perfil</a></li>
                    <li><a href="#">Privacidad</a></li>
                </ul>
            </li>
            <li class="active"> <a href="{{ route('auth/logout') }}">Cerrar Sesion</a></li>
        </ul
    </section>

    @else

    <section class="top-bar-section">
        <ul class="right">
            <li class="active"> <a href="{{ route('user/login') }}">Autentificacion de Usuario</a></li>
        </ul>
    </section>

    @endif

</nav>