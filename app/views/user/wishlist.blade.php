@extends ('layout')

@extends('menu')

@section ('title') Lista de Deseos @stop

@section ('content')

<script src="{{ asset('assets/js/modules/user/wishlist.js') }}"></script>

<br>

<h1 class="text-center">Regalos de Obsequios</h1>

<h4 class="subheader text-center">Lista de Presentes Pendientes para {{ $user->username }}</h4>

<br>
<!-- TABLA  PENDIENTES -->
@if (count($wishs) == 0)
<div>

    <h4>No has añadido ningun deseo aún.</h4>

    <img src="{{ asset('assets/img/meditacion.jpg') }}" width="100" height="250">

    <br>

    <medium>Liberate de todo deseo decia buda...</medium>

    <br>

</div>
@else

<table id="pendientes" class="large-12 columns text-left">
    <tr>
        <th>Nombre</th>

        <th>Descripcion</th>

        <th>Precio</th>

        <th></th>

    </tr>

    @foreach ($wishs as $wish)

    @if ($wish->WishStatus->status == 1)
    <tr>

        <td>{{ $wish->description }}</td>

        <td>{{ $wish->reference }}</td>

        <td>{{ $wish->price }}</td>

        <td>

            @if (Auth::check())

            <img src="{{ asset('assets/img/buttons/regalo.gif') }}" data-id="{{ $wish->id }}" class="btnEstado" width="50" heigth="50">

            <a href="{{ route('wish/show', $wish->id) }}">
                <img src="{{ asset('assets/img/buttons/lupa.png') }}" width="50" heigth="50" >
            </a>


            @else

            <a href="{{ route('user/sign-up')}}">Cambiar Estado</a>

            @endif

        </td>
    </tr>
    @endif

    @endforeach

</table>

<!-- END TABLA PENDIENTES -->

<br>

@endif

<hr>
<br>

<h1 class="text-center">Regalos Actualmente Adquiridos</h1>

<h4 class="subheader text-center">Lista de Presentes adquiridos por otros amigos de {{ $user->username }}</h4>

<br>

<!-- TABLA  COMPRADOS -->
<table id="comprados" class="large-12 columns text-left">

    <thead>

    <!--<tr class="filaoculta">-->
    <tr>

        <th>Descripcion</th>

        <th>Referencia</th>

    </tr>

    </thead>

    <tbody>

    <tr id="sinCompras" class="text-center">

        <td colspan="2">Oops! Nadie ha comprado algo de la lista aun, se el primero.</td>

    </tr>

    @if ($wish->WishStatus->status == 0)

    <tr>

        <td>{{ $wish->description }}</td>

        <td>{{ $wish->reference }}</td>

    </tr>

    @endif

    </tbody>

</table>

<!-- END TABLA COMPRADOS -->

<br>

<!-- MENSAJES -->
<div id="myModal" class="reveal-modal text-center" data-reveal>

    <h2>Has conseguido el regalo!.</h2>

    <p>Gracias por tomarte el tiempo de cumplir mis deseos :)</p>

    <img src="{{ asset('assets/img/comprado.jpg') }}" width="120" heigth="100">

    <a class="close-reveal-modal">&#215;</a>

</div>

<a href="#" data-reveal-id="myModal" data-reveal></a>
<!-- MENSAJES END -->
<br>
<br>
<br>
<br>
@stop