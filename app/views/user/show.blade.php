@extends ('layout')

@extends ('menu')

@section ('title') Mi Perfil @stop

@section ('content')

<div class="row">

    <div class="large-12 columns">

        <h2>Mi Perfil</h2>

    </div>

</div>

<br>

<!-- DATOS -->
<div class="row">

    <div class="small-6 columns text-center">

        <div class="avatar">

            <img src="{{ asset($userImage->path) }}" />

        </div>

    </div>

    <div class="small-6 columns text-left">

        <br>
        <br>

        <div class="row">

            <div class="small-5 columns">Nombre:</div>

            <div class="small-5 columns">

                <label>{{ $user->name }} {{ $user->lastname }}</label>

            </div>

        </div>

        <br>

        <div class="row">

            <div class="small-5 columns">Ubicacion:</div>

            <div class="small-5 columns">

                <label>Santiago, Chile</label>

            </div>

        </div>

        <br>

        <div class="row">

            <div class="small-5 columns">Correo:</div>

            <div class="small-5 columns">

                <label>{{ $user->email }}</label>

            </div>

        </div>

    </div>

</div>
<!-- FIN DATOS -->

<hr>
<br>
<br>

<div class="row">

    @if (count($wishlists) == 0)

    <h4>{{ $user->name }} no ha añadido ninguna lista aun ¿Deseas hacerle sugerencias?</h4>

    <img src="{{ asset('assets/img/meditacion.jpg') }}" width="100" height="250">

    <br>

    @else

    <h3 class="text-left">Listas disponibles:</h3>

    <!-- TABLA  -->
    <table id="pendientes" class="small-12 columns text-left">

        <thead>

        <tr>

            <th class="small-9 columns text-center">Nombre</th>

            <th class="small-3 columns text-center">Ver</th>

        </tr>

        </thead>

        <tbody>

        @foreach ($wishlists as $wishlist)
        <tr>

            <td class="small-9 columns text-left">{{  $wishlist->name }}</td>

            <td class="small-3 columns text-left">

                <a href="{{ route('user/wishlist', $wishlist->id) }}">

                    <img width="50" heigth="50" src="{{ asset('assets/img/buttons/lupa.png') }}">

                </a>

            </td>

        </tr>

        @endforeach

        </tbody>

    </table>

    @endif

</div>

<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script>
    $(document).foundation();
</script>
<script src="{{ asset('assets/js/modules/user/show.js') }}"></script>

@stop