@extends ('layout')

@section ('title') Crear Lista @stop

@section ('content')

@if (!Auth::check())

<div class="row text-center">

    <div class="small-12 colums">

        <br>
        <hr>
        <br>

        <!-- FORMULARIO -->
        {{ Form::open(['route' => 'auth/login', 'method' => 'POST', 'role' => 'form', 'novalidate', 'data-abide']) }}

            <div class="row">

                <div class="large-12 columns">

                    <h1>Autentificacion de Usuario</h1>

                </div>

            </div>

            <div class="row">

                <div class="small-6 large-centered columns">

                    @if (Session::has('login_error'))
                        <label>Credenciales NO Validas</label>
                    @endif

                    <label>Usuario</label>
                    <input type="text" placeholder="Ingresa username o tu correo" id="username" name="username" required data-invalid>
                    <small class="error">Nombre o Correo es requerido.</small>

                    <br>

                    <label>Contraseña</label>
                    <input type="password" placeholder="Ingresa tu contraseña" id="password" name="password"/>
                    <small class="error">Contraseña es requerida.</small>

                    <br>

                    <div id="alertPass" data-alert class="alert-box warning round">
                        Si no recuerdas tu contraseña presiona sobre el siguiente enlace.
                        <a href="#" class="close">&times;</a>
                    </div>
                    <p>¿Olvidaste tu contraseña? <a href="http://www.youtube.com/watch?v=zT2aVoUkSDg">Recupararla</a></p>

                </div>

            </div>

            <br>

            <div class="small-12 columns">

                <button class="big round button" type="submit">Ingresar</button>

            </div>

        {{ Form::close() }}
        <!-- END FORMULARIO -->

    </div> <!-- end 12 columnas -->

</div> <!-- end row -->

@else

    <div class="small-12 columns">

        <h1>Te estamos redirigiendo a la Página Principal :)</h1>

    </div>
    {{ Redirect::route('home') }}

@endif

<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script>
    $(document).foundation();
</script>

@stop