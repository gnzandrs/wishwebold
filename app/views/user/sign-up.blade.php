@extends('layout')

@section('content')

<!-- REGISTRO -->
<div class="row">

  <div class="small-12 columns">

    <h2>Registrate para comenzar!</h2>

  </div>

</div>


{{ Form::open(['route' => 'register', 'method' => 'POST', 'role' => 'form', 'novalidate', 'data-abide']) }}

  <div class="small-6 large-centered columns text-left">

      @include ('user/errors', array('errors' => $errors))

    <div class="row">

        <div data-alert class="alert-box success radius filaoculta" id="usuarioDisponible">
            El nombre de usuario se encuentra disponible!
            <a href="#" class="close">&times;</a>
        </div>

        <div data-alert class="alert-box alert round filaoculta" id="usuarioNoDisponible">
            Alerta: Éste nombre de usuario ya se encuentra registrado, escoge otro.
            <a href="#" class="close">&times;</a>
        </div>

        <label><h4>Nombre de usuario</h4></label>

         <input type="text" placeholder="Ingrese un nombre de usuario" id="username" name="username" maxlength="20" required data-abide-validator="valUsername"/>

         <small class="error">El nombre de usuario es requerido.</small>

    </div>

    <br>

    <div class="row">

        <label><h4>Nombre</h4></label>

        <input type="text" placeholder="Ingrese su nombre" id="name" name="name" maxlength="20" required/>

        <small class="error">El nombre es requerido.</small>

    </div>

    <br>

    <div class="row">

        <label><h4>Apellido</h4></label>

        <input type="text" placeholder="Ingrese su apellido" id="lastname" name="lastname" maxlength="20" required/>

        <small class="error">El apellido es requerido.</small>

    </div>

    <br>

    <div class="row">

        <div data-alert class="alert-box alert round filaoculta" id="correoNoDisponible">
            Alerta: Éste correo ya se encuentra registrado, utiliza uno diferente.
            <a href="#" class="close">&times;</a>
        </div>

        <label><h4>Email</h4></label>

        <input type="email" placeholder="Ingrese su correo" id="email" name="email" maxlength="30" required data-abide-validator="valEmail"/>

        <small class="error">El correo es requerido.</small>

    </div>

    <br>

    <div class="row">

        <label><h4>Genero</h4></label>

        <input type="radio" value="male" id="genreMale" id="genre" name="genre" required> <label>Masculino</label>

        <input type="radio" value="female" id="genreFemale" id="genre" name="genre" required><label>Femenino</label>

    </div>

    <div class="row">

          <label>País</label>

          <select id="country" name="country" required data-invalid>

              <option value>-Selecciona tu País-</option>
              @foreach ($countrys as $country)
              <option value="{{ $country->id }}">{{ $country->name }}</option>
              @endforeach
          </select>

          <small class="error">La Pais es requerido.</small>

    </div>


      <div id="citiesSelect" class="row">

      </div>

    <div class="row">

        <label><h4>Contraseña</h4></label>

        <input type="password" placeholder="Ingrese su contraseña" id="password" name="password" required/>

        <small class="error">Una contraseña es requerida.</small>

    </div>

    <br>

    <div class="row">

        <label><h4>Confirmar Contraseña</h4></label>

        <input type="password" placeholder="Ingrese su contraseña" id="password_confirmation" name="password_confirmation" required data-equalto="password"/>

        <small class="error">La contraseña no coincide.</small>

    </div>

    <br>
    <br>
    <br>

    <div class="row text-center">
      
      <button  type="submit" class="big round button">Aceptar</button>
    
    </div>
  
  </div>

  </form> <!-- END REGISTRO -->

{{ Form::close() }}

<script src="{{ asset('assets/js/modules/user/sign-up.js') }}"></script>

@endsection