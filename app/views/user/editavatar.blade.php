<link rel="stylesheet" href="{{ asset('assets/css/juploadfile/uploadfile.css') }}" />
<script src="{{ asset('assets/js/juploadfile/jquery.uploadfile.js') }}"></script>
<script src="{{ asset('assets/js/modules/user/editavatar.js') }}"></script>
<style type="text/css">
    /* Apply these styles only when #preview-pane has
       been placed within the Jcrop widget */
    .jcrop-holder #preview-pane {
        display: block;
        position: absolute;
        z-index: 2000;
        top: 10px;
        right: -280px;
        padding: 6px;
        border: 1px rgba(0,0,0,.4) solid;
        background-color: white;

        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;

        -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
        box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
    }

    /* The Javascript code will set the aspect ratio of the crop
       area based on the size of the thumbnail preview,
       specified here */
    #preview-pane .preview-container {
        width: 250px;
        height: 170px;
        overflow: hidden;
    }
</style>

<div id="subirImagen" class="row">

    <div class="small-12 columns text-center">

        <center>

            <div id="mulitplefileuploader">Seleccionar Imagen</div>

            <div id="status"></div>

        </center>
    </div>

</div>


<div id="recortarImagen" class="row filaoculta">
</div>


