@extends ('layout')

@extends('menu')

@section ('title') Configuracion Perfil @stop

@section ('content')

@include ('utils/alert/saveresult')

@include ('utils/error/errors', array('errors' => $errors))

@include ('utils/breadcrumb/userconfig')

<form data-abide>

    <div class="row">

        <div class="small-8 small-centered columns text-center">

            <br>

            <div class="avatar">

                <img src="{{ asset($userImage->path) }}?1.1.0" />

            </div>

            <br>
            <br>

            <div class="row">

                <div class="row">

                    <div class="small-12 columns">

                        <a id="btnAvatar" class="button tiny">Cambiar Imagen</a>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <label>Nombre</label>

                <input type="text" placeholder="Ingrese su nombre" value="{{ $user->name }}" id="name" name="name" required data-invalid/>

                <small class="error">El nombre es requerido.</small>

            </div>

            <br>

            <div class="row">

                <label>Apellido</label>

                <input type="text" placeholder="Ingrese su apellido" value="{{ $user->lastname }}" id="lastname" name="lastname" required data-invalid/>

                <small class="error">El apellido es requerido.</small>

            </div>

            <br>

            <div class="row">

                <label>Correo</label>

                <input type="text" placeholder="Ingrese su correo electronico" value="{{ $user->email }}" id="email" name="email" required data-invalid/>

                <small class="error">El email es requerido.</small>

            </div>

            <br>

            <div class="row">

                <label><h4>Genero</h4></label>

                @if ($user->genre == 'male')
                <input type="radio" value="male" name="genre" required checked> <label>Masculino</label>
                <input type="radio" value="female" name="genre" required><label>Femenino</label>
                @else
                <input type="radio" value="male" name="genre" required> <label>Masculino</label>
                <input type="radio" value="female" name="genre" required checked><label>Femenino</label>
                @endif

            </div>

            <br>

            <div class="row">

                <label>Pais</label>

                <select id="country" name="country" required data-invalid>

                    <option value>-Selecciona tu Pais-</option>
                    @foreach ($countries as $country)
                    <option value="{{ $country->id }}"
                        @if ($country->id == $user->city->country->id )
                        selected
                        @endif >{{ $country->name }}
                    </option>
                    @endforeach

                </select>

                <small class="error">El pais es requerido.</small>

            </div>

            <div id="citiesSelect" class="row">

                <div class="row">

                    <label>Ciudad</label>

                    <select id="city_id" name="city" required data-invalid>

                        <option value>-Selecciona tu Ciudad-</option>
                        @foreach ($cities as $city)
                            @if ($city->country_id == $user->city->country->id)
                                <option value="{{ $city->id }}"
                                @if ($city->id == $user->city->id )
                                selected
                                @endif >{{ $city->name }}
                                </option>
                            @endif
                        @endforeach

                    </select>

                    <small class="error">El pais es requerido.</small>

                </div>

            </div>

            <br>
            <br>
            <br>

            <div class="row text-center">

                <input type="button" id="btnGuardar" class="big round button" value="Guardar"><br/>

            </div>

        </div> <!-- END 6 COLUMNS -->

    </div>

</form>

<!-- MODAL DESEO -->
<div id="modalAvatar" class="reveal-modal text-center" data-reveal>
</div>


<script>
    $(document).foundation();
</script>
<script src="{{ asset('assets/js/modules/user/profile.js') }}"></script>

@stop