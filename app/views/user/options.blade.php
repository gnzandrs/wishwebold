@extends ('layout')

@extends('menu')

@section ('title') Opciones de Configuracion @stop

@section ('content')

@include ('utils/alert/saveresult')

@include ('utils/error/errors', array('errors' => $errors))

@include ('utils/breadcrumb/userconfig')

<div class="row">

    <div class="small-8 small-centered columns text-center">

        <br>

        <label><h4>Notificaciones por correo: </h4></label>

        @if ($user->configuration->notificacion == 1)
        <input type="radio" value="1" name="notification" required checked> <label>Si</label>
        <input type="radio" value="0" name="notification" required><label>No</label>
        @else
        <input type="radio" value="1" name="notification" required> <label>Si</label>
        <input type="radio" value="0" name="notification" required checked><label>No</label>
        @endif


        <br>
        <br>
        <br>

        <div class="row text-center">

            <input type="button" id="btnGuardar" class="big round button" value="Guardar"><br/>

        </div>

    </div> <!-- END 6 COLUMNS -->

</div> <!-- END ROW -->

<script src="{{ asset('assets/js/modules/user/options.js') }}"></script>

@stop