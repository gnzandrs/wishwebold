<!doctype html>
<html class="no-js" lang="es">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title', 'SmartWish')</title>
    <link rel="stylesheet" href="{{ asset('assets/css/foundation.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}" />
    <!--<link rel="stylesheet" href="{{ asset('assets/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />-->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui/jquery-ui.structure.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui/jquery-ui.theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" />
    <script src="{{ asset('assets/js/vendor/modernizr.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/jquery.js') }}"></script> 
    <script src="{{ asset('assets/js/vendor/jquery-ui.js') }}"></script>
    <script src="{{ asset('assets/js/foundation.min.js') }}"></script>
  </head>
  <body>

  <!-- CONTENIDO -->
  <div class="row text-center">

    <div class="small-12 columns">

      @yield('content')

    </div> <!-- END 12 COLUMNS -->

  </div>

  <!-- PIE DE PAGINA -->
  @include ('footer')

  <script>
    $(document).foundation();
  </script>
  </body>
</html>
