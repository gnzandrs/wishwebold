@extends ('layout')

@extends('menu')

@section ('title') Crear Lista @stop

@section ('content')

@include ('utils/alert/saveresult')

@include ('utils/error/errors', array('errors' => $errors))

<style>
#map{
    display: block;
    width: 100%;
    height: 350px;
    margin: 4em auto;
}
</style>
<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/css/dropzone/basic.css') }}" />
<script src="{{ asset('assets/js/foundation/foundation.reveal.js') }}"></script>
<script src="{{ asset('assets/js/modules/wishlist/create.js') }}"></script>

<form id="frmValidaciones" data-abide="ajax">

    <div class="row">

        <div class="large-12 columns">

            <h2>Crear Lista</h2>

        </div>

    </div>

    <!-- DIV 6 COLUMNAS -->
    <div class="small-6 large-centered columns text-left">

        <label>Nombre</label>

        <input type="text" id="name" name="name" placeholder="Escribe un nombre para la lista"  required data-invalid/>

        <small class="error">El nombre es requerido.</small>

        <label>Privacidad</label>

        <select disabled required data-invalid>

            <option value>-Selecciona el tipo-</option>

            <option value="personal">Personal</option>

            <option value="protected">Privada</option>

            <option value="public" selected>Publica</option>

        </select>

        <small class="error">El pais es requerido.</small>


    </div>
    <!-- FIN DIV 6 COLUMNAS -->

</form>

<!-- DIV 12 COLUMNAS -->
<div class="large-12 columns">

    <hr>

    <div id="btnAnadir" class="large-12 columns text-right">

        <div data-alert class="alert-box info radius large-11 columns text-center">

            Has click en el boton para añadir deseos a tu lista.

            <a href="#" class="close">&times;</a>

        </div>

        <img id="btnAnadir" src="{{ asset('assets/img/buttons/anadir.png') }}">

    </div>


    <hr>

    <!-- DESEOS -->
    <div id="listaDeseos">
    </div>
    <!-- FIN DESEOS -->

</div>


<!-- PARAMETROS -->
<input type="hidden" name="hdWishListId" id="hdWishListId" value="0">

<!-- MODAL DESEO -->
<div id="myModal" class="reveal-modal xlarge text-center" data-reveal data-options="close_on_background_click:false; close_on_esc: false;"></div>
</div>
<!-- FIN MODAL DESEO -->

<!-- libreria gmaps js -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ asset('assets/js/gmaps.js') }}"></script>
<!-- FIN DIV PRINCIPAL -->

@stop