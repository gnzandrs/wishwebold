@extends ('layout')

@extends ('menu')

@section ('title') Mis Listas @stop

@section ('content')

@include ('utils/alert/saveresult')

@include ('utils/error/errors', array('errors' => $errors))

<div class="small-12 columns">

    <h2>Mis Listas</h2>

</div>

<!-- LISTAS -->
<div class="row">

    <br>

    @if (count($wishlists) == 0)

    <h4>No has añadido ninguna lista aun.</h4>

    <img src="{{ asset('assets/img/meditacion.jpg') }}" width="100" height="250">

    <br>

    @else

    <table id="pendientes" class="small-12 columns">

        <thead>

        <tr>

            <th class="small-9 columns text-center">Nombre</th>

            <th class="small-3 columns text-center">Ver</th>

        </tr>

        </thead>

        <tbody>

        @foreach ($wishlists as $wishlist)
        <tr>

            <td class="small-9 columns">{{  $wishlist->name }}</td>

            <td class="small-3 columns">

                <a href="{{ route('wishlist/edit', $wishlist->id) }}">

                    <img width="50" heigth="50" src="{{ asset('assets/img/buttons/editar.png') }}">

                </a>

                <a href="{{ route('user/wishlist', $wishlist->id) }}">

                    <img width="50" heigth="50" src="{{ asset('assets/img/buttons/lupa.png') }}">

                </a>

                <a data-id="{{ $wishlist->id }}" class="btnEliminar">

                    <img width="50" heigth="50" src="{{ asset('assets/img/buttons/quitar.png') }}">

                </a>

                <a data-id="{{ $wishlist->id }}" class="btnEnlace">

                    <img width="50" heigth="50" src="{{ asset('assets/img/buttons/Chain.png') }}">

                </a>

            </td>

        </tr>
        @endforeach

        </tbody>

    </table>

    @endif

</div>

<!-- MENSAJES -->
<div id="modalEnlance" class="reveal-modal text-center" data-reveal>

    <h2>Copia y envia el siguiente enlace a tus amigos!</h2>

    <div class="small-12 columns">

        <div class="small-12 columns text-center">

            <div class="small-2 columns">
                <br>
            </div>
            <div class="small-8 columns">
                <input type="url" id="urlLista" name="urlLista" maxlength="20" style="font-size: 30px;" disabled/>
            </div>
            <div class="small-2 columns">
                <br>
            </div>

        </div>

        <div class="small-12 columns text-center">

            <img src="{{ asset('assets/img/sociales.png') }}" >

        </div>

   </div>


    <a class="close-reveal-modal">&#215;</a>

</div>

<a href="#" data-reveal-id="modalEnlance" data-reveal></a>
<!-- MENSAJES END -->
<input type="hidden" id="hdUrl" name="hdUrl" value="{{ URL::to('/') }}" />

<script src="{{ asset('assets/js/modules/wishlist/show.js') }}"></script>
<script>
    $(document).foundation();
</script>


@stop