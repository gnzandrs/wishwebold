<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/vendor/jquery.mask.js') }}"></script>
<script>
$('#price').mask('0.000.000', {reverse: true});
</script>
<link rel="stylesheet" href="{{ asset('assets/css/dropzone/basic.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/dropzone/dropzone.css') }}" />
<div id="deseo">

    <div class="row">

        <div class="small-6 columns"></div>

        <div class="small-6 colums">

            <a class="close-reveal-modal">&#215;</a>

        </div>

    </div>

    <div class="small-6 columns text-center">

        <div class="row">

            <form class="dropzone" id="aw" style="width: 400px; height: 300px;">
            </form>

        </div>

        <div class="row">

            </br>

        </div>

    </div>

    <form id="frmDeseo" data-abide="ajax">

        <div class="small-6 columns">

            <br>
            <br>

            <div class="row">

                <div class="small-3 columns">Descripcion:</div>

                <div class="small-9 columns">

                    <input type="text" id="description" name="description" placeholder="Ingresa el nombre del objeto" required data-invalid/>

                    <small class="error">La descripcion es requerida.</small>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="small-3 columns">Referencia:</div>

                <div class="small-9 columns">

                    <input type="text" id="reference" name="reference" placeholder="Ingresa la referencia del objeto" required data-invalid/>

                    <small class="error">La referencia es requerida.</small>

                </div>

            </div>

            </br>

            <div class="row">

                <div class="small-3 columns">Precio:</div>

                <div class="small-9 columns">

                    <input type="text" id="price" name="price" placeholder="Ingresa un precio aproximado" required data-invalid/>

                    <small class="error">El precio es requerido.</small>

                </div>

            </div>

            </br>

        </div>
        <!-- FIN COLUMNA -->
    </form>

    <div class="small-12 columns">

        Categoria:

    </div>

    <!-- 12 COLUMNAS -->
    <div class="small-12 columns text-center">

        <br>

        @foreach ($categories as $category)
        <a id="cat_{{ $category->id }}" data-id="{{ $category->id }}" class="Radius Secondary Label categoryLabel">{{ $category->name }}</a>
        @endforeach

        <br>

    </div>

    <div class="small-12 columns">

        Ubicacion:

    </div>

    <!-- 12 COLUMNAS -->
    <div class="small-12 columns text-center">

        </br>

        <!-- buscar direccion
        <div id="buscar">

            <div class="row">

                <div class="medium-12 columns text-center">

                    <input type="text" id="busqueda" placeholder="Buscar sitio..." />

                    <input type="button" class="small round button" id="btnBuscar" value="Buscar">

                </div>

            </div>

        </div>
         fin buscar direccion -->

        </br>

        <div data-alert class="alert-box info radius">
            Selecciona en el mapa el lugar puntual donde se puede adquirir el objeto, o tambien puedes utilizar un lugar
            señalado anteriormente por otro usuario con la informacion del lugar ya descrita.
            <a href="#" class="close">×</a>
        </div>

        <!-- MAPA -->
        <div id="map">
        </div>

        <!-- localizaciones recientes -->
        <!-- fin localizaciones recientes -->

        <!--  OCULTOS -->
        <a id="btnSave" name="btnSave"></a>
        <a id="btnMapa" name="btnMApa"></a>
        <input type="hidden" name="hdIdLocation" id="hdIdLocation" value="0">
        <input type="hidden" name="hdIdList" id="hdIdList" value="{{ $wish->list_id }}">
        <input type="hidden" name="hdIdCategory" id="hdIdCategory" value="0">

        <a id="btnGuardarDeseo" class="big round button">Guardar Deseo</a>

    </div>
    <!-- FIN 12 COLUMNAS -->

</div>


<script src="{{ asset('assets/js/modules/wishlist/wish.js') }}"></script>