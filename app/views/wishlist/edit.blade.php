@extends ('layout')

@extends('menu')

@section ('title') Editar Lista @stop

@section ('content')

@include ('utils/alert/saveresult')

@include ('utils/error/errors', array('errors' => $errors))

@if (count($wishlist) == 0)
 <div class="row">
    No se ha especificado una lista a editar.
 </div>
@else
<style>
    #map{
        display: block;
        width: 100%;
        height: 350px;
        margin: 4em auto;
    }
</style>
<script src="{{ asset('assets/js/foundation/foundation.joyride.js') }}"></script>
<script src="{{ asset('assets/js/vendor/jquery.cookie.js') }}"></script>
<script src="{{ asset('assets/js/foundation/foundation.reveal.js') }}"></script>
<script src="{{ asset('assets/js/modules/wishlist/edit.js') }}"></script>

<form id="frmValidaciones" data-abide="ajax">
    <div class="row">

        <div class="large-12 columns">

            <h2>Editar Lista</h2>

        </div>

    </div>

    <!-- DIV 6 COLUMNAS -->
    <div class="small-6 large-centered columns text-left">

        <label>Nombre</label>

        <input type="text" id="name" name="name" placeholder="Escribe un nombre para la lista"  value="{{ $wishlist->name }}" required data-invalid/>

        <small class="error">El nombre es requerido.</small>

        <label>Privacidad</label>

        <select disabled required data-invalid>

            <option value>-Selecciona el tipo -</option>

            <option value="personal">Personal</option>

            <option value="protected">Privada</option>

            <option value="public" selected>Publica</option>

            <!--@if ($wishlist->access == 'public')
                <option value="personal">Personal</option>
                <option value="protected">Privada</option>
                <option value="public" selected>Publica</option>
            @elseif  ($wishlist->access == 'protected')
                <option value="personal" selected>Personal</option>
                <option value="protected">Privada</option>
                <option value="public">Publica</option>
            @elseif  ($wishlist->access == 'private')
                <option value="personal">Personal</option>
                <option value="protected" selected>Privada</option>
                <option value="public">Publica</option>
            @endif
            -->
        </select>

        <small class="error">El tipo es requerido.</small>


    </div>
    <!-- FIN DIV 6 COLUMNAS -->

</form>

<!-- DIV 12 COLUMNAS -->
<div class="large-12 columns">

    <hr>

    <div id="btnAnadir" class="large-12 columns text-right">

        <div data-alert class="alert-box info radius large-11 columns text-center">

            Has click en el boton para añadir deseos a tu lista.

            <a href="#" class="close">&times;</a>

        </div>

        <img id="btnAnadir" src="{{ asset('assets/img/buttons/anadir.png') }}">

    </div>


    <hr>

    <!-- DESEOS -->
    <div id="listaDeseos">
    </div>
    <!-- FIN DESEOS -->

</div>


<!-- PARAMETROS -->
<input type="hidden" name="hdWishListId" id="hdWishListId" value="{{ $wishlist->id }}">

<!-- MODAL DESEO -->
<div id="myModal" class="reveal-modal xlarge text-center" data-reveal data-options="close_on_background_click:false; close_on_esc: false;"></div>
</div>
<!-- At the bottom of your page but inside of the body tag -->
<ol class="joyride-list" data-joyride>
    <li data-id="name" data-text="Next" data-options="tip_location: top; prev_button: false">
        <p>Hello and welcome to the Joyride <br>documentation page.</p>
    </li>
    <li data-id="numero1" data-class="custom so-awesome" data-text="Next" data-prev-text="Prev">
        <h4>Stop #1</h4>
        <p>You can control all the details for you tour stop. Any valid HTML will work inside of Joyride.</p>
    </li>
</ol>
<!-- FIN MODAL DESEO -->

<!-- libreria gmaps js -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ asset('assets/js/gmaps.js') }}"></script>
<script>//$(document).foundation('joyride', 'start');</script>
<!-- FIN DIV PRINCIPAL -->
@endif

@stop