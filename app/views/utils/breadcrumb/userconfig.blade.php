<div class="row">

    <div class="small-6 small-centered columns text-center">

        <br>

        <h1>Configuracion</h1>

        <br>

        <ul id="ulMenu" class="breadcrumbs">

            <li><a href="{{ route('user/profile') }}">Perfil</a></li>

            <li><a href="#">Privacidad</a></li>

            <li><a href="{{ route('user/option') }}">Opciones</a></li>

        </ul>

        <br>

    </div>

</div>

<input type="hidden" id="hdUrl" value="{{ Request::url() }}" />