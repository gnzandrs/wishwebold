<script>
$( ".wishMenu" ).menu({
    items: "> :not(.ui-widget-header)"
});

$('.li').click(function(){
    var id = $(this).data('id');
    guardarDeseo(id)
});

$('.creaLista').click(function(){

    var wishId = $('#wishId').val();
    // modal deseo
    $('#myModal').foundation('reveal', 'open', {
        url: 'wishlist/createWithWish',
        data: { wishId: wishId },
        type: 'POST',
        dataType: 'html'
    });

});

function guardarDeseo(listId)
{
    var wishId = $('#wishId').val();

    //alert('lista: ' + );
    $.ajax({
        url: '/wish/copy',
        type: 'POST',
        data: { wishId: wishId, wishListId: listId },
        dataType: 'html',
        success: function(respuesta) {
            if (respuesta == 1) {
                alert("Deseo agregado con exito");
            } else {
                alert("Error al cargar listas");
            }
        }
    });
}

</script>
<input type="hidden" id="wishId" name="wishId" value="1"/>
<ul class="wishMenu">
    <li class="ui-widget-header">Agregar a Lista</li>
    @foreach ($wishlists as $wishlist)
        <li class="li" data-id="{{ $wishlist->id }}">{{ $wishlist->name }}</li>
    @endforeach
    <li class="creaLista">Crear lista...</li>
</ul>

<!-- MODAL DESEO -->
<div id="myModal" class="reveal-modal text-center" data-reveal></div>
</div>