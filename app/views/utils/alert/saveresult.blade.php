<div class="row">
    <div data-alert class="alert-box success radius filaoculta" id="guardarOk">
        Los cambios han sido aplicados con exito!
        <a href="#" class="close">&times;</a>
    </div>

    <div data-alert class="alert-box alert round filaoculta" id="guardarError">
        Alerta: Se ha producido un error al guardar los cambios.
        <a href="#" class="close">&times;</a>
    </div>
</div>