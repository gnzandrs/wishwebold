@extends ('layout')

@section ('title') Crear Lista @stop

@section ('content')

<script>
$(function() {

    var list_id = $('#hdWishListId').val();

    // añadir lista
    $('#btnGuardarLista').click(function(){
        $('#myModal').foundation('reveal', 'close');
    });

    function cargarListaDeseos()
    {
        // llamada al controlador
        $.ajax({
            url: '/wish/wishlistmodal',
            data: { id: list_id },
            type: 'POST',
            dataType: 'html',
            async: false,
            beforeSend: function() {
                null;
            },
            error: function() {
                null;
            },
            success: function(respuesta) {
                if (respuesta) {
                    $('#listaDeseos').html(respuesta);
                } else {
                    alert("Error al cargar la lista de deseos");
                }
            }
        });
    }

    cargarListaDeseos();
});
</script>

<form id="frmValidaciones" data-abide="ajax">
    <div class="row">

        <div class="large-12 columns">

            <h2>Nueva Lista</h2>

        </div>

    </div>

    <!-- DIV 6 COLUMNAS -->
    <div class="small-6 large-centered columns text-left">

        <label>Nombre</label>

        <input type="text" id="name" name="name" placeholder="Escribe un nombre para la lista"  value="{{ $wishlist->name }}" required data-invalid/>

        <small class="error">El nombre es requerido.</small>

        <label>Privacidad</label>

        <select disabled required data-invalid>

            <option value>-Selecciona el tipo -</option>

            <option value="personal">Personal</option>

            <option value="protected">Privada</option>

            <option value="public" selected>Publica</option>

        </select>

        <small class="error">El tipo es requerido.</small>


    </div>
    <!-- FIN DIV 6 COLUMNAS -->

</form>

<!-- DIV 12 COLUMNAS -->
<div class="large-12 columns">

    <hr>

    <!-- DESEOS -->
    <div id="listaDeseos">
    </div>
    <!-- FIN DESEOS -->

</div>

<a id="btnGuardarLista" class="big round button">Seguir Mirando!</a>

<!-- PARAMETROS -->
<input type="hidden" name="hdWishListId" id="hdWishListId" value="{{ $wishlist->id }}">

@stop