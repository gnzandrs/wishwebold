<script src="{{ asset('assets/js/jcrop/jquery.Jcrop.js') }}"></script>
<script src="{{ asset('assets/js/modules/user/cropimage.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/css/jcrop/jquery.Jcrop.css') }}" />
<p>
    <h3>Ajustar Imagen</h3>
</p>

<div id="mensajeAlerta" data-alert class="alert-box alert round filaoculta">
    Atención: Debe recortar la imagen
    <a href="#" class="close">&times;</a>
</div>

<div class="small-6 columns text-center">

    <center> <img id="target" src="{{ asset($userImage->path) }}"> </center>

</div>

<div class="small-6 columns text-center">

    <div id="preview-pane">

        <div class="preview-container">

            <center>

                <img src="{{ asset($userImage->path) }}" class="jcrop-preview" alt="Preview" />

            </center>

        </div>

    </div>

</div>

<br>

<div class="row">

    <div class="small-12 columns">

        <a id="btnAceptar" class="button tiny">Aceptar</a>

    </div>

</div>

<input type="hidden" id="x" name="x" value="-1"/>
<input type="hidden" id="y" name="y" value="-1"/>
<input type="hidden" id="w" name="w" value="-1"/>
<input type="hidden" id="h" name="h" value="-1"/>
<script>
    $(document).foundation(); // fix modal
</script>