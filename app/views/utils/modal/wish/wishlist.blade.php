@if (count($wishs) == 0)
<div>

    <h4>No has añadido ningun deseo aún.</h4>

    <img src="{{ asset('assets/img/meditacion.jpg') }}" width="100" height="250">

    <br>

    <medium>Liberate de todo deseo decia buda...</medium>

    <br>

</div>
@else
<table class="table table-striped">
    <tr>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Precio</th>
        <th></th>
    </tr>
    @foreach ($wishs as $wish)
    <tr>
        <td>{{ $wish->description }}</td>
        <td>{{ $wish->reference }}</td>
        <td>{{ $wish->price }}</td>
        <td>
        </td>
    </tr>
    @endforeach
</table>

@endif
