<div class="row">
    @if ($errors->any())
    <div class="panel callout radius">
        <strong><h5>Por favor corrige los siguentes errores:</h5></strong>
            @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
            @endforeach
    </div>
    @endif
</div>