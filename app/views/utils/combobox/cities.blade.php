<label>Ciudad</label>

<select id="city_id" name="city_id" required data-invalid>

    <option value>-Selecciona tu Ciudad-</option>

    @foreach ($cities as $city)

    <option value="{{ $city->id }}">{{ $city->name }}</option>

    @endforeach

</select>

<small class="error">La region es requerido.</small>