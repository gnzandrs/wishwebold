@extends ('layout')

@section ('title') Listas de deseos para ti y tus Amigos @stop

<!-- Imagen Portada -->
<div id="header" class="home-hero">
    <div class="row">
        <div class="text-center">
            <br><br><br><br>
            <br><br><br><br>
            <h1 style="color: #FFF;">Smart Gift</h1>
            <h2 style="color: #FFF;">Crea, Comparte, Obsequia!</h2>
        </div>
    </div>
</div>
<!-- End Imagen Portada -->

@section ('content')

<!-- CONTENIDO -->
<div class="row text-center">

    <div class="small-12 colums">

        <br>
        <hr>
        <br>

        <!-- Introduccion -->

        <h2>Crea y comparte tu lista de intereses con el mundo.</h2>

        <h4 class="subheader">Captura, Comparte y Compra lo que que realmente desees y necesites!</h4>
        <p>
            Imagina archivar ese objeto que viste alguna vez y no pudiste comprar en aquel momento.
            ¿No seria genial poder guardar todos tus deseos hasta que puedas obtenerlo, o mejor aun: Que alguien lo obtenga por ti?
            ¿Porque pasar horas y horas inventando un regalo a esa persona que tal vez ni siquiera le agrade ni necesite?<br>
            Mejor regala algo que esa misma persona desea recibir y se encuentre a tu alcance de obsequiárselo.
        </p>

        <!-- end Introduccion -->


        <!-- Caracteristicas -->
        <div class="small-4 columns">
            <p>
                <a href="#">
                    <img src="{{ asset('assets/img/home/intro/tomarfoto.png') }}">
                    <h4>Captura</h4>
                </a>
                <br />
                Captura ese objeto que es de tu interes con una fotografia que además determine geograficamente en donde obtenerlo.
            </p>
        </div>

        <div class="small-4 columns">
            <p>
                <a href="#">
                    <img src="{{ asset('assets/img/home/intro/explica.png') }}">
                    <h4>Comparte</h4>
                </a>
                <br />
                Comparte en una lista ese objeto con tus amigos.
            </p>
        </div>

        <div class="small-4 columns">
            <p>
                <a href="#">
                    <img src="{{ asset('assets/img/home/intro/compras.png') }}" width="250" heigth="250">
                    <h4>Tiempo de Compras</h4>
                </a>
                <br />
                Ha llegado el momento en que puedes obtenerlo, o mejor aun ¿Se dio la oportunidad que otras personas te lo puedes obsequiar?
                ¡Excelente! Tienen su catalogo de compras listo en base de tus deseos e intereses.
            </p>

        </div>
        <!-- end Caracteristicas -->

        <hr>
        <div class="row text-center">

            <a  href="{{ route('user/sign-up') }}" class="big round button expand">Registrarse!</a>

            <a  href="{{ route('user/login') }}" class="big round button expand">¿Ya tienes una cuenta?</a>
        </div>

    </div> <!-- END 12 COLUMNS -->

</div> <!-- END CONTENIDO -->

<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script>
    $(document).foundation();
</script>

@stop
