@extends ('layout')

@extends('menu')

@section ('title') Listas de deseos para ti y tus Amigos @stop

@section ('content')

<!-- BUSQUEDA -->
<div class="row">

    <div class="small-9 small-centered columns text-center">

        <br>

        <h1>Busqueda</h1>

        <input type="text" id="search" placeholder="Ingresa un patron de búsqueda..." required />

    </div>

    <div class="small-4 small-centered columns text-center">

        <a  id="btnBuscar" class="small round button expand">Buscar</a>

        <br>

    </div>

</div>
<!-- END BUSQUEDA -->

<h1>¡Encuentra lo que buscas!</h1>

<br>
    <!-- CATEGORIA -->
    @foreach ($categories as $category)
        <a href="{{ route('category', $category->name) }}" class="Radius Secondary Label">{{ $category->name }}</a>
    @endforeach
    <!-- END CATEGORIA -->
<br>

<hr>

<br>

<div id="barraCarga" class="filaoculta"><div class="progress-label">Buscando...</div></div>

<div id="deseos">
</div>

<script src="{{ asset('assets/js/modules/home/home.js') }}"></script>
<script src="{{ asset('assets/js/progressbar.js') }}"></script>
@stop