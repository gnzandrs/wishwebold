<ul class="small-block-grid-3">
    @foreach ($wishs as $wish)
    <li>
        <div class="img_thumb">
            <div class="img_desc">
                <p>{{ $wish->description }}</p>
            </div>
            <a href="{{ route('wish/show', $wish->id) }}"><img src="{{ asset($wish->WishImages->first()->thumb_path) }}"></a>
        </div>
    </li>
    @endforeach
</ul>