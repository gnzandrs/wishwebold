@extends ('layout')

@extends('menu')

@section ('title') Listas de deseos para ti y tus Amigos @stop

@section ('content')

<script src="{{ asset('assets/js/modules/category/category.js') }}"></script>
<!-- BUSQUEDA -->
<div class="row">

    <div class="small-9 small-centered columns text-center">

        <br>

        <input type="hidden" id="hdCategory" value="{{ $name }}" />

        <input type="text" id="search" placeholder="Ingresa un patron de búsqueda..." required />

    </div>

    <div class="small-4 small-centered columns text-center">

        <a  id="btnBuscar" class="small round button expand">Buscar</a>

        <br>

    </div>

</div>
<!-- END BUSQUEDA -->

<hr>

<div id="barraCarga" class="filaoculta"><div class="progress-label">Buscando...</div></div>

<div id="deseos">

    <ul class="small-block-grid-3">

        @foreach ($wishs as $wish)
        <li>

            <div class="img_thumb">

                <div class="img_desc">

                    <p>{{ $wish->description }}</p>

                </div>

                <a href="{{ route('wish/show', $wish->id) }}"><img src="{{ asset($wish->WishImages->first()->thumb_path) }}"></a>

            </div>

        </li>
        @endforeach

    </ul>

</div>
<script src="{{ asset('assets/js/progressbar.js') }}"></script>
@stop