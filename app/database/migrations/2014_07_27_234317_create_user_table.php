<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username')->unique();
			$table->string('name');
			$table->string('lastname');
			$table->string('email')->unique();
			$table->enum('type', ['user' ,'admin']);
			$table->string('genre');
			$table->boolean('active');
			$table->string('password');
			$table->string('slug');
			$table->string('remember_token')->nullable();
            $table->integer('city_id')->unsigned();

            $table->foreign('city_id')->references('id')->on('city');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
