<?php

class CountryTableSeeder extends Seeder {

	public function run()
	{
        Wish\Entities\Country::create([
            'id' => 1,
            'name' => 'Chile',
            'code' => 'CL'
        ]);

        Wish\Entities\Country::create([
            'id' => 2,
            'name' => 'Argentina',
            'code' => 'AR'
        ]);

        Wish\Entities\Country::create([
            'id' => 3,
            'name' => 'Peru',
            'code' => 'PER'
        ]);

        Wish\Entities\Country::create([
            'id' => 4,
            'name' => 'Colombia',
            'code' => 'COL'
        ]);
	}

}