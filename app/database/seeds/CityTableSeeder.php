<?php

class CityTableSeeder extends Seeder {

	public function run()
	{
        Wish\Entities\City::create([
            'id' => 1,
            'name' => 'Santiago',
            'code' => '02',
            'country_id' => 1
        ]);

       Wish\Entities\City::create([
            'id' => 2,
            'name' => 'Mendoza',
            'code' => '03',
            'country_id' => 2
        ]);

        Wish\Entities\City::create([
            'id' => 3,
            'name' => 'Lima',
            'code' => '03',
            'country_id' => 3
        ]);

        Wish\Entities\City::create([
            'id' => 4,
            'name' => 'Bogota',
            'code' => '04',
            'country_id' => 4
        ]);
	}

}