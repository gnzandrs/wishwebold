<?php

class CategoryTableSeeder extends Seeder {

	public function run()
	{
        Wish\Entities\Category::create([
            'id' => 1,
            'name' => 'Fotografia'
        ]);

        Wish\Entities\Category::create([
            'id' => 2,
            'name' => 'Computacion'
        ]);

        Wish\Entities\Category::create([
            'id' => 3,
            'name' => 'Ropa'
        ]);

        Wish\Entities\Category::create([
            'id' => 4,
            'name' => 'Libros'
        ]);
	}

}