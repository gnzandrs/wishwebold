<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is the place where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
| Author: @gnzandrs

*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

// Category
Route::get('category/{name}', ['as' => 'category', 'uses' => 'CategoryController@show']);
Route::get('category/search/{category}/{search}', ['as' => 'category/search', 'uses' => 'CategoryController@searchByCategory']);

// Wish
Route::get('wish/show/{id}', ['as' => 'wish/show', 'uses' => 'WishController@show']);
Route::get('search/{search}', ['as' => 'search', 'uses' => 'WishController@search']);
Route::post('wish/store', ['as' => 'wish/store', 'uses' => 'WishController@store']);
Route::post('wish/copy', ['as' => 'wish/copy', 'uses' => 'WishController@copy']);
Route::post('wish/edit', ['as' => 'wish/edit', 'uses' => 'WishController@edit']);
Route::post('wish/update', ['as' => 'wish/update', 'uses' => 'WishController@update']);
Route::post('wish/destroy', ['as' => 'wish/destroy', 'uses' => 'WishController@destroy']);
Route::post('wish/wishlist', ['as' => 'wish/wishlist', 'uses' => 'WishController@wishList']);
Route::post('wish/wishlistmodal', ['as' => 'wish/wishlistmodal', 'uses' => 'WishController@wishListModal']);
Route::post('wish/imageList', ['as' => 'wish/imageList', 'uses' => 'WishController@imageList']);
Route::post('wish/imageUpload', ['as' => 'wish/imageupload', 'uses' => 'WishController@imageUpload']);
Route::post('wish/imageDelete', ['as' => 'wish/imageDelete', 'uses' => 'WishController@imageDelete']);
Route::post('wish/imagesDelete', ['as' => 'wish/imagesDelete', 'uses' => 'WishController@imagesDelete']);
Route::post('wish/changeStatus', ['as' => 'wish/changestatus', 'uses' => 'WishController@changeStatus']);
Route::post('wish/latestAdded', ['as' => 'wish/latestAdded', 'uses' => 'WishController@latestAdded']);

// Wish List
Route::get('wishlist/create', ['as' => 'wishlist/create', 'uses' => 'WishListController@create']);
Route::post('wishlist/createWithWish', ['as' => 'wishlist/createWithWish', 'uses' => 'WishListController@createWithWish']);
Route::get('wishlist/show', ['as' => 'wishlist/show', 'uses' => 'WishListController@show']);
Route::get('wishlist/edit/{id}', ['as' => 'wishlist/edit', 'uses' => 'WishListController@edit']);
Route::post('wishlist/list/user', ['as' => 'wishlist/list/user', 'uses' => 'WishListController@wishListByUser']);
Route::post('wishlist/store', ['as' => 'wishlist/store', 'uses' => 'WishListController@store']);
Route::post('wishlist/destroy', ['as' => 'wishlist/destroy', 'uses' => 'WishListController@destroy']);
Route::post('wishlist/wishShow', ['as' => 'wishlist/wishshow', 'uses' => 'WishListController@wishShow']);


// User
Route::get('user', ['as' => 'user', 'uses' => 'UserController@index']);
Route::get('user/sign-up', ['as' => 'user/sign-up', 'uses' => 'UserController@signUp']);
Route::get('user/check/{username}', ['as' => 'user/check', 'uses' => 'UserController@check']);
Route::get('user/checkEmail/{email}', ['as' => 'user/checkEmail', 'uses' => 'UserController@checkEmail']);
Route::get('user/login', ['as' => 'user/login', 'uses' => 'UserController@login']);
Route::get('user/show/{id}', ['as' => 'user/show', 'uses' => 'UserController@show']);
Route::get('user/wishlist/{id}', ['as' => 'user/wishlist', 'uses' => 'UserController@wishListShow']);
Route::post('user/register', ['as' => 'register', 'uses' => 'UserController@register']);
Route::post('user/citiesList', ['as' => 'user/citiesList', 'uses' => 'UserController@citiesList']);
Route::post('user/avatarImage', ['as' => 'user/avatarImage', 'uses' => 'UserController@getAvatarImage']);

// User configurations
Route::get('user/profile', ['as' => 'user/profile', 'uses' => 'ConfigurationController@profile']);
Route::post('user/profile/edit', ['as' => 'user/profile/edit', 'uses' => 'ConfigurationController@updateProfile']);
Route::post('user/profile/editAvatar', ['as' => 'user/profile/editAvatar', 'uses' => 'ConfigurationController@editAvatar']);
Route::post('user/profile/imageUpload', ['as' => 'user/profile/imageUpload', 'uses' => 'ConfigurationController@imageUploadProfile']);
Route::post('user/profile/imageCrop', ['as' => 'user/profile/imageCrop', 'uses' => 'ConfigurationController@imageCrop']);
Route::post('user/profile/imageCropApply', ['as' => 'user/profile/imageCropApply', 'uses' => 'ConfigurationController@imageCropApply']);
Route::get('user/option', ['as' => 'user/option', 'uses' => 'ConfigurationController@option']);
Route::post('user/option/edit', ['as' => 'user/option/edit', 'uses' => 'ConfigurationController@updateOption']);

// User Login
Route::post('auth/login', array("as" => 'auth/login', 'uses' => 'AuthController@login'));
Route::get('auth/logout', array("as" => 'auth/logout', 'uses' => 'AuthController@logout'));

// Location
Route::get('location/show/{id}', array("as" => 'location/show', 'uses' => 'LocationController@show'));
Route::get('location/search/{lat}/{lng}', array("as" => 'location/search', 'uses' => 'LocationController@search'));
Route::post('location/store', array("as" => 'location/store', 'uses' => 'LocationController@store'));
Route::post('location/getMarkers', array("as" => 'location/getMarkers', 'uses' => 'LocationController@getMarkers'));
Route::post('location/getLocation', array("as" => 'getLocation', 'uses' => 'LocationController@getLocation'));
Route::post('location/getLocationByWish', array("as" => 'getLocationByWish', 'uses' => 'LocationController@getLocationByWish'));

//Logs
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

