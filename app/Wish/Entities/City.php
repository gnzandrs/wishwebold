<?php namespace Wish\Entities;

use Whoops\Example\Exception;

class City extends \Eloquent {
    protected $table = 'city';
    protected $fillable = ['name','code'];

    public function country()
    {
        return $this->belongsTo('Wish\Entities\Country', 'country_id','id');
    }
}