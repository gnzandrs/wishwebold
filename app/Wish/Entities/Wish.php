<?php namespace Wish\Entities;

use Whoops\Example\Exception;

class Wish extends \Eloquent {
	protected $table = 'wish';
	protected $fillable = ['description','reference','price', 'list_id', 'location_id', 'category_id'];
    protected $perPage = 3;

	public function wishlist()
	{
		return $this->belongsTo('Wish\Entities\WishList', 'list_id','id');
	}

    public function location()
    {
        return $this->belongsTo('Wish\Entities\Location');
    }

    public function wishimages()
    {
        return $this->hasMany('Wish\Entities\WishImage', 'wish_id', 'id');
    }

    public function wishstatus()
    {
        return $this->hasOne('Wish\Entities\WishStatus', 'wish_id', 'id');
    }

    public function category()
    {
        return $this->hasOne('Wish\Entities\Category', 'id', 'category_id');
    }

}