<?php namespace Wish\Entities;

use Whoops\Example\Exception;

class Country extends \Eloquent {
    protected $table = 'country';
    protected $fillable = ['name','code'];

    public function city()
    {
        return $this->hasMany('Wish\Entities\City', 'country_id', 'id');
    }
}