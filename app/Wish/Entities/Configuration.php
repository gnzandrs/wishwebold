<?php namespace Wish\Entities;

class Configuration extends \Eloquent {

    protected $table = 'configuration';
    protected $fillable = array('notificacion', 'deal');

	public function user()
	{
		return $this->belongsTo('Wish\Entities\User', 'user_id', 'id');
	}
}