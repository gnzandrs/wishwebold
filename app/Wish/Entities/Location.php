<?php namespace Wish\Entities;

class Location extends \Eloquent {
    protected $table = 'location';
    protected $fillable = ['name','latitude','longitude'];

    public function wishs()
    {
        return $this->hasMany('Wish\Entities\Wish', 'location_id', 'id');
    }
}