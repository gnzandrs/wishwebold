<?php namespace Wish\Entities;

class WishList extends \Eloquent {
	protected $table = 'list';
	protected $fillable = ['name'];

	public function user()
	{
		return $this->belongsTo('Wish\Entities\User', 'user_id', 'id');
	}

	public function wishs()
	{
		return $this->hasMany('Wish\Entities\Wish', 'list_id', 'id');
	}

    public function getPaginateWishsAttribute()
    {
        return  Wish::where('list_id', $this->id)->paginate();
    }
}