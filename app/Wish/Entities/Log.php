<?php namespace Wish\Entities;
/**
 * User: @gnzandrs
 * Date: 21-06-15
 * Time: 08:40 PM
 * Desc: This class is used to insert a log in the BD every time a exception happen in the code.
 */

class Log extends \Eloquent {
    protected $table = 'log';
    protected $fillable = ['file','class','description', 'exception'];
} 