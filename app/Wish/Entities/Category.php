<?php namespace Wish\Entities;

class Category extends \Eloquent {
    protected $table = 'category';
    protected $fillable = ['name'];

    public function wishs()
    {
        return $this->hasMany('Wish\Entities\Wish', 'category_id', 'id');
    }
}