<?php namespace Wish\Entities;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends \Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $fillable = array('username', 'name', 'lastname', 'email', 'genre', 'city_id', 'password');
    public $errors;

	public function configuration()
    {
        return $this->hasOne('Wish\Entities\Configuration', 'user_id', 'id');
    }

    public function city()
    {
        return $this->hasOne('Wish\Entities\City', 'id', 'city_id');
    }

    public function setPasswordAttribute($value)
    {
        if (!empty($value))
        {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function lists()
    {
    	return $this->hasMany('Wish\Entities\List');
    }

    public function UserImage()
    {
        return $this->hasOne('Wish\Entities\UserImage', 'user_id', 'id');
    }

}
