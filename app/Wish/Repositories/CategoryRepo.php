<?php

namespace Wish\Repositories;

use Whoops\Example\Exception;
use Wish\Entities\Category;

class CategoryRepo extends BaseRepo {

    public function getModel()
    {
        return new Category;
    }

    public function getCategories()
    {
        return Category::all();
    }

}