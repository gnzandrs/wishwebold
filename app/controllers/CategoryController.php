<?php

use Wish\Entities\Wish;
use Wish\Managers\WishManager;
use Wish\Repositories\WishRepo;
use Wish\Entities\Log;
use Wish\Repositories\LogRepo;

class CategoryController extends BaseController {

    protected $wishRepo;
    protected $logRepo;

    public function __construct(WishRepo $wishRepo, LogRepo $logRepo)
    {
        $this->wishRepo = $wishRepo;
        $this->logRepo = $logRepo;
    }

    // destroy the category in db
    public function destroy()
    {

    }

    //
    public function edit()
    {

    }

    // get all the categories
    public function getCategories()
    {
        try{
            if (Auth::check())
            {
                if (Request::ajax())
                 {
                    $wishs = $this->wishRepo->latestAdded();
                    return View::make('wish/latest', compact('wishs'));
                }
            }
            else{
                return Redirect::route('user/login');
            }
        }
        catch(Exception $e)
        {
            Log::error('CategoryController getCategories(): '.$e);
            $this->logRepo->newLog('CategoryController.php', 'CategoryController.php', 'error catch', $e);
            return 0;
        }
    }

    // redirect to index
    public function index()
    {
        try {
            Log::warning('CategoryController index');
            return Redirect::route('home');
        }
        catch (Exception $e)
        {
            Log::error('CategoryController index: '.$e);
            $this->logRepo->newLog('CategoryController.php', 'CategoryController.php', 'error catch', $e);
            App::abort(500);
        }
    }

    // search the record by one category and typed text
    public function searchByCategory($category, $search)
    {
        try{
            Log::info("CategoryController searchByCategory($category, $search)");
            $wishs = $this->wishRepo->getListByCategorySearch($category, $search);

            if (Request::ajax())
            {
                return View::make('category/wishlist', compact('wishs'));
            }
        }
        catch(Exception $e)
        {
            Log::error('CategoryController searchByCategory($category, $search): '.$e);
            $this->logRepo->newLog('CategoryController.php', 'CategoryController.php', 'error catch', $e);
            return 0;
        }

    }

    // view of category
    public function show($name)
    {
        try{
            Log::info("CategoryController show($name)");
            $wishs = $this->wishRepo->getListByCategory($name);
            return View::make('category/category', compact('wishs', 'name'));
        }
        catch(Exception $e)
        {
            Log::error('CategoryController show($name): '.$e);
            $this->logRepo->newLog('CategoryController.php', 'CategoryController.php', 'error catch', $e);
            return 0;
        }

    }

    public function store()
    {

    }

    public function update()
    {

    }

}