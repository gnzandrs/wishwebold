<?php

use Wish\Managers\ImageManager;

class ImageController extends BaseController {
    protected $imageManager;

    public function __construct(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    // receive the image from client and save it in a folder
    public function postUpload()
    {
        try {
            //$file = Input::file('image');
            $file = Input::file('image');
            /*$input = array('image' => $file);
            $rules = array(
                'image' => 'image'
            );
            $validator = Validator::make($input, $rules);
            if ( $validator->fails() )
            {
                return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);

            }*/
            //else {
            $destinationPath = 'uploads/';
            $filename = $file->getClientOriginalName();
            Input::file('image')->move($destinationPath, $filename);
            return Response::json(['success' => true, 'file' => asset($destinationPath.$filename)]);
            //}

        }
        catch (Exception $e)
        {
            Log::error('ImageController postUpload: '.$e);
            $this->logRepo->newLog('ImageController.php', 'ImageController.php', 'error catch', $e);
            return 0;
        }
    }

    // return the upload image form
    public function getUploadForm()
    {
        try {
            return View::make('image/upload-form');
        }
        catch (Exception $e)
        {
            Log::error('ImageController getUploadForm(): '.$e);
            $this->logRepo->newLog('ImageController.php', 'ImageController.php', 'error catch', $e);
            return 0;
        }
    }

} 