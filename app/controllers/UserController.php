<?php

use Wish\Entities\User;
use Wish\Entities\Country;
use Wish\Entities\City;
use Wish\Entities\WishList;
use Wish\Managers\RegisterManager;
use Wish\Managers\UserManager;
use Wish\Repositories\UserRepo;
use Wish\Repositories\ConfigurationRepo;
use Wish\Repositories\WishListRepo;
use Wish\Entities\Log;
use Wish\Repositories\LogRepo;


class UserController extends BaseController {

	protected $userRepo;
    protected $configRepo;
    protected $wishlistRepo;
    protected $logRepo;

    public function __construct(UserRepo $userRepo, ConfigurationRepo $configRepo,
                                WishListRepo $wishlistRepo, LogRepo $logRepo)
	{
        $this->configRepo = $configRepo;
        $this->userRepo = $userRepo;
        $this->wishlistRepo = $wishlistRepo;
        $this->logRepo = $logRepo;
	}

    // check availability of username
    public function check($username)
    {
        try{
            Log::info('UserController check($username)');
            $available = $this->userRepo->userCheck($username);
            if (Request::ajax())
            {
                return $available;
            }
        }
        catch (Exception $e)
        {
            Log::error('UserController check($username): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }

    // check availability of email
    public function checkEmail($email)
    {
        try{
            $available = $this->userRepo->emailCheck($email);
            if (Request::ajax())
            {
                return $available;
            }
        }
        catch (Exception $e)
        {
            Log::error('UserController emailCheck($email): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }

    // return cities list
    public function citiesList()
    {
        try{
            $countryCode = Input::get('code');
            $cities = DB::table('city')
                ->join('country', 'city.country_id', '=', 'country.id')
                ->where('country.id', $countryCode)
                ->select('city.id', 'city.code', 'city.name')
                ->get();

            if (Request::ajax())
            {
                return View::make('utils/combobox/cities', compact('cities'));
            }
        }
        catch (Exception $e)
        {
            Log::error('UserController citiesList(): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }

    // get user avatar by id
    public function getAvatarImage()
    {
        try{
            $userId = Auth::user()->id;
            $image = $this->userRepo->getAvatarImage($userId);
            if (Request::ajax())
            {
                return $image;
            }
        }
        catch (Exception $e)
        {
            Log::error('UserController getAvatarImage(): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }

    // return to home
    public function index()
    {
        try{
            Log::info("UserController index");
            return Redirect::route('home');
        }
        catch (Exception $e)
        {
            Log::error('UserController index(): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }

    }

    // return login view
    public function login()
    {
        return View::make('user/login');
    }

    // record a new user
    public function register()
    {
        try{
            $user = $this->userRepo->newUser();
            $manager = new RegisterManager($user, Input::all());
            $result = $manager->save();
            $result = $this->userRepo->createDirectoryTree($user);

            if($result)
            {
                return Redirect::route('home');
            }
            else{
                return Redirect::route('user/sign-up')->withInput()->withErrors($user->errors);
            }
        }
        catch (Exception $e)
        {
            Log::error('UserController register(): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }

    // return user view
    public function show($id)
    {
        try{
            $user = $this->userRepo->find($id);
            $userImage = $this->configRepo->userImageById($user->id);
            $wishlists = $this->userRepo->wishLists($id);
            return View::make('user/show', compact('user', 'wishlists', 'userImage'));
        }
        catch (Exception $e)
        {
            Log::error('UserController show($id): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }

    // sign-up view
    public function signUp()
    {
        try{
            $countrys = Country::all();
            return View::make('user/sign-up', compact('countrys'));
        }
        catch (Exception $e)
        {
            Log::error('UserController signUp(): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }

    // public view of the lists
    public function wishListShow($id)
    {
        try{
            $wishlist = $this->wishlistRepo->find($id);
            $wishs = $wishlist->Wishs;
            $user = $wishlist->User;
            return View::make('user/wishlist', compact('wishs', 'user'));
        }
        catch (Exception $e)
        {
            Log::error('UserController wishListShow($id): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }
}
