<?php

use Wish\Repositories\CategoryRepo;

class HomeController extends BaseController {

	protected $categoryRepo;

	public function __construct(CategoryRepo $categoryRepo)
	{
		$this->categoryRepo = $categoryRepo;
	}

    // return index view of the site
	public function index()
	{
		try {
            Log::info('HomeController index');
            //$latest_wishs = $this->wishRepo->findLatest();
            //return View::make('home', compact('latest_wishs'));
            $categories = $this->categoryRepo->getCategories();

            if (Auth::check())
            {
                return View::make('home/home', compact('categories'));
            }
            else
            {
                return View::make('home/intro');
            }
        }
        catch (Exception $e)
        {
            Log::error('HomeController index: '.$e);
            $this->logRepo->newLog('HomeController.php', 'HomeController.php', 'error catch', $e);
            return 0;
        }
	}

}
