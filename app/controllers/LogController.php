<?php

use Wish\Entities\Log;
use Wish\Managers\LogManager;
use Wish\Repositories\LogRepo;

class LogController extends BaseController {

    protected $logRepo;

    public function __construct(LogRepo $logRepo)
    {
        $this->logRepo = $logRepo;
    }

    // redirect to index
    public function index()
    {
        return Redirect::route('home');
    }

    // save log into database
    public function store()
    {
        try {
            $log = $this->logRepo->newLog();
            $manager = new RegisterManager($log, Input::all());
            $result = $manager->save();
        }
        catch(exception $e)
        { }
    }

}
