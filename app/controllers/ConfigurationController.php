<?php

use Wish\Entities\User;
use Wish\Entities\Country;
use Wish\Entities\City;
use Wish\Managers\RegisterManager;
use Wish\Managers\OptionManager;
use Wish\Managers\UserManager;
use Wish\Repositories\UserRepo;
use Wish\Repositories\ConfigurationRepo;
use Wish\Entities\Log;
use Wish\Repositories\LogRepo;

class ConfigurationController extends BaseController {

    protected $configRepo;
    protected $userRepo;
    protected $logRepo;

    public function __construct(UserRepo $userRepo, ConfigurationRepo $configRepo, LogRepo $logRepo)
    {
        $this->configRepo = $configRepo;
        $this->userRepo = $userRepo;
        $this->logRepo = $logRepo;
    }

    // edit's view of user avatar
    public function editAvatar()
    {
        try {
            return View::make('user/editavatar');
        }
        catch (Exception $e)
        {
            Log::error('ConfigurationController editAvatar(): '.$e);
            $this->logRepo->newLog('ConfigurationController.php', 'ConfigurationController.php', 'error catch', $e);
            return 0;
        }
    }

    // redirect to index
    public function index()
    {
        try {
            Log::info("ConfigurationController index");
            return Redirect::route('home');
        }
        catch (Exception $e)
        {
            Log::error('ConfigurationController index(): '.$e);
            $this->logRepo->newLog('ConfigurationController.php', 'ConfigurationController.php', 'error catch', $e);
            return 0;
        }
    }

    // cut image view
    public function imageCrop()
    {
        try {
            $userId = Auth::user()->id;
            $userImage = $this->configRepo->userImageById($userId);
            return View::make('utils/modal/user/cropimage', compact('userImage'));
        }
        catch (Exception $e)
        {
            //Log::error('ConfigurationController imageCrop(): '.$e);
            $this->logRepo->newLog('ConfigurationController.php', 'ConfigurationController.php', 'error catch', $e);
            return 0;
        }
    }

    // apply cut to image
    public function imageCropApply()
    {
        try {
            $x =  Input::get('x');
            $y =  Input::get('y');
            $w =  Input::get('w');
            $h =  Input::get('h');
            $width = Input::get('width'); // original width
            $height = Input::get('height'); // original height
            $userId = Auth::user()->id;
            $result = $this->configRepo->userImageCropApply($userId, $width, $height, $x, $y, $w, $h);

            if (Request::ajax())
            {
                if($result)
                {
                    return 1;
                }
                else{
                    return 0;
                }
            }
        }
        catch (Exception $e)
        {
            //Log::error('ConfigurationController imageCropApply(): '.$e);
            $this->logRepo->newLog('ConfigurationController.php', 'ConfigurationController.php', 'error catch', $e);
            return 0;
        }
    }

    // upload user's profile image
    public function imageUploadProfile()
    {
        try {
            if (Auth::check())
            {
                $file = Input::file('file');
                $upload_success = $this->configRepo->newUserImage($file, Auth::user()->id);

                if( $upload_success ) {
                    return Response::json('success', 200);
                } else {
                    return Response::json('error', 400);
                }
            }
            else{
                return Redirect::route('user/login');
            }
        }
        catch (Exception $e)
        {
            //Log::error('ConfigurationController imageUploadProfile(): '.$e);
            $this->logRepo->newLog('ConfigurationController.php', 'ConfigurationController.php', 'error catch', $e);
            return 0;
        }
    }

    // user's options view
    public function option()
    {
        try {
            if (Auth::check())
            {
                $id = Auth::user()->id;
                $user = $this->userRepo->find($id);
                $config = $user->configuration;
                return View::make('user/options', compact('config', 'user'));
            }
            else{
                return Redirect::route('user/login');
            }
        }
        catch (Exception $e)
        {
            Log::error('ConfigurationController option(): '.$e);
            $this->logRepo->newLog('ConfigurationController.php', 'ConfigurationController.php', 'error catch', $e);
            return 0;
        }
    }

    //
    public function privacy()
    {

    }

    // user's profile view
    public function profile()
    {
        try {
            if (Auth::check())
            {
                $id = Auth::user()->id;
                $user = $this->userRepo->find($id);
                //$conf = $this->userRepo->getConfig($userId);
                $config = $user->configuration;
                $userImage = $this->configRepo->userImageById($user->id);
                $countries = Country::all();
                $cities = City::all();
                return View::make('user/profile', compact('config', 'user', 'countries', 'cities', 'userImage'));
            }
            else{
                return Redirect::route('user/login');
            }
        }
        catch (Exception $e)
        {
            Log::error('ConfigurationController profile(): '.$e);
            $this->logRepo->newLog('ConfigurationController.php', 'ConfigurationController.php', 'error catch', $e);
            return 0;
        }
    }

    // update user's option
    public function updateOption()
    {
        try {
            $user = Auth::user();
            $configuration = $user->configuration;
            $manager = new OptionManager($configuration, Input::all());
            if (Request::ajax())
            {
                if($manager->save())
                {
                    return 1;
                }
                else{
                    return Redirect::back()->withInput()->withErrors($user->errors);
                }
            }
        }
        catch (Exception $e)
        {
            Log::error('ConfigurationController updateOption(): '.$e);
            $this->logRepo->newLog('ConfigurationController.php', 'ConfigurationController.php', 'error catch', $e);
            return 0;
        }
    }

    public function updatePrivacy()
    {

    }

    // update user's profile
    public function updateProfile()
    {
        try {
            //Log::info('ConfigurationController updateProfile()');
            $user = Auth::user();
            $manager = new UserManager($user, Input::all());

            if (Request::ajax())
            {
                if($manager->save())
                {
                    return 1;
                }
                else{
                    return Redirect::back()->withInput()->withErrors($user->errors);
                }
            }
        }
        catch (Exception $e)
        {
            //Log::error('ConfigurationController updateProfile(): '.$e);
            $this->logRepo->newLog('ConfigurationController.php', 'ConfigurationController.php', 'error catch', $e);
            return 0;
        }
    }

}